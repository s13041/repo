package servlets;

import repositories.IRepoCatalog;
import repositories.impl.RepoCatalogProvider;
import domain.Person;

public class PersonServletLogic {

	IRepoCatalog catalog;
	
	public PersonServletLogic() {
		catalog = RepoCatalogProvider.catalog();
	}
	
	public void addNewPerson(HttpServletRequest request)
	{
		Person a = new Person();
		a.setPesel(request.getParameter("pesel"));
		a.setFirstName(request.getParameter("firstName"));
		catalog.getPersons().save(a);
		catalog.commit();
	}
	
	public String showPersonInhtmlForm()
	{
		String html = "<ol>";
		for(Person u: catalog.getPersons().getAll())
		{
			html+="<li>"
					+ u.getPesel()
					+ "</li>";
		}
		html+="</ol>";
		return html;
	}
}