package servlets;

import java.io.IOException;


@WebServlet("/AddRole")
public class AddRoleServlets extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	RoleServletLogic logic = new RoleServletLogic();
	
    public AddRoleServlets() {
        super();
    }

	protected void doPost(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException {

		logic.addNewRole(request);
		response.sendRedirect("ShowRole");
	}

}
