package servlets;

import java.io.IOException;


@WebServlet("/AddPerson")
public class AddPersonServlets extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	PersonServletLogic logic = new PersonServletLogic();
	
    public AddPersonServlets() {
        super();
    }

	protected void doPost(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException {

		logic.addNewPerson(request);
		response.sendRedirect("ShowPerson");
	}

}
