package servlets;

import java.io.IOException;

@WebServlet("/ShowPrivilege")
public class ShowPrivilegeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	PrivilegeServletLogic logic = new PrivilegeServletLogic();
    public ShowPrivilegeServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");
		response.getWriter().print(logic.showPrivilegeInhtmlForm());
		
	}
}