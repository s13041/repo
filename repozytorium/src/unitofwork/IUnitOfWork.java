package unitofwork;

import domain.Entity;

public interface IUnitOfWork {

	public void commit();
	public void rollback();
	public void markAsNew(Entity entity, IUnitOfWorkRepo Repo);
	public void markAsDirty(Entity entity, IUnitOfWorkRepo Repo);
	public void markAsDeleted(Entity entity, IUnitOfWorkRepo Repo);
}
