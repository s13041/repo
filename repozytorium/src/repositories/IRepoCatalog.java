package repositories;

import domain.*;

public interface IRepoCatalog {

	public IUserRepo getUsers();
	public IRepo<Person> getPersons();
	public IRepo<Role> getRoles();
	public void commit();
	IRepo<Address> getAddress();
	IRepo<Privilege> getPriviliges();
}
