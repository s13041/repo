package repositories.impl;

import java.sql.Connection;

import domain.Address;
import domain.Person;
import domain.Privilege;
import domain.Role;
import repositories.IRepo;
import repositories.IRepoCatalog;
import repositories.IUserRepo;
import unitofwork.IUnitOfWork;

public class RepoCatalog implements IRepoCatalog{

	private Connection connection;
	private IUnitOfWork uow;
	
	public RepoCatalog(Connection connection, IUnitOfWork uow) {
		super();
		this.connection = connection;
		this.uow = uow;
	}

	@Override
	public IUserRepo getUsers() {
		return new UserRepo(connection, new UserMake(), uow);
	}

	@Override
	public IRepo<Person> getPersons() {
		return new PersonRepo(connection, new PersonMake(), uow);
	}

	@Override
	public IRepo<Role> getRoles() {
		return new RoleRepo(connection, new RoleMake(), uow);
	}
	
	@Override
	public IRepo<Address> getAddress() {
		return new AddressRepo(connection, new AddressMake(), uow);
	}
	
	@Override
	public IRepo<Privilege> getPriviliges() {
		return new PrivilegeRepo(connection, new PreviligeMake(), uow);
	}

	@Override
	public void commit() {
		uow.commit();
	}

}
