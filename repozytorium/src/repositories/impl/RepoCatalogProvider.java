package repositories.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import repositories.IRepoCatalog;
import unitofwork.IUnitOfWork;
import unitofwork.UnitOfWork;

public class RepoCatalogProvider {


	private static String url="jdbc:hsqldb:hsql://localhost/workdb";
	
	public static IRepoCatalog catalog()
	{

		try {
			Connection connection = DriverManager.getConnection(url);
			IUnitOfWork uow = new UnitOfWork(connection);
			IRepoCatalog catalog = new RepoCatalog(connection, uow);
		
			return catalog;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
